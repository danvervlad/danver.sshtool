﻿using System.Collections.Generic;
using ConsoleApplication2.Dto;
using ConsoleApplication2.Job;
using ConsoleApplication2.Stage;

namespace ConsoleApplication2.Pack
{
    public class PackFactory : IPackFactory
    {
        private readonly IStageFactory _stageFactory;
        private readonly IOperationFactory _operationFactory;

        public PackFactory(IStageFactory stageFactory, IOperationFactory operationFactory)
        {
            _stageFactory = stageFactory;
            _operationFactory = operationFactory;
        }

        public PackDto CreatePack(PackRawDto packRawDto)
        {
            var stage = _stageFactory.CreateStageDto(packRawDto.StageJsonFile);

            var operations = new List<OperationDto>(packRawDto.OperationsJsonFiles.Count);
            for (int j = 0; j < packRawDto.OperationsJsonFiles.Count; j++)
            {
                operations.Add(_operationFactory.CreateOperation(packRawDto.OperationsJsonFiles[j]));
            }

            return new PackDto(stage, operations);
        }
    }
}