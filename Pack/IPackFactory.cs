﻿using ConsoleApplication2.Dto;

namespace ConsoleApplication2.Pack
{
    public interface IPackFactory
    {
        PackDto CreatePack(PackRawDto packRawDto);
    }
}