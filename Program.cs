﻿using System;
using System.Diagnostics;
using System.IO;
using ConsoleApplication2.Job;
using ConsoleApplication2.JobRuner;
using ConsoleApplication2.Json;
using ConsoleApplication2.Logger;
using ConsoleApplication2.Operation;
using ConsoleApplication2.Pack;
using ConsoleApplication2.Stage;
using ConsoleApplication2.Template;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            var jsonDeserializer = new JsonDeserializer();
            var sessionOptionsFactory = new SessionOptionsFactory();
            var operationFactory = new OperationFactory(jsonDeserializer);
            var stageFactory = new StageFactory(jsonDeserializer);
            var packFactory = new PackFactory(stageFactory, operationFactory);
            var jobFactory = new JobFactory(jsonDeserializer, packFactory);
            var logger = new ConsoleLogger();
            var jobRunner = new JobRunner(sessionOptionsFactory, logger);

            if (args.Length == 0 || string.IsNullOrEmpty(args[0]))
            {
                logger.WriteLine("No Args are passed!");
                var exampleCreator = new ExampleCreator(logger);
                exampleCreator.CreateTemplates();

                Console.ReadLine();
                return;
            }

            var jobFilePath = args[0];
            try
            {
                if (!File.Exists(jobFilePath))
                {
                    throw new InvalidOperationException(string.Format("App Argument: File '{0}' doesn't exist.", jobFilePath));
                }

                var jobDto = jobFactory.CreateJob(jobFilePath);
                jobRunner.RunJob(jobDto);
            }
            catch (Exception e)
            {
                logger.WriteLine(string.Format("App Error: {0}", e.Message));

                if (Debugger.IsAttached)
                {
                    Debugger.Break();
                }
            }

            Console.ReadLine();
        }
    }
}
