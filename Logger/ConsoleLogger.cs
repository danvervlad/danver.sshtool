﻿using System;

namespace ConsoleApplication2.Logger
{
    public class ConsoleLogger : ILogger
    {
        public void WriteLine(string logText)
        {
            Console.WriteLine(logText);
        }
    }
}