﻿namespace ConsoleApplication2.Logger
{
    public interface ILogger
    {
        void WriteLine(string logText);
    }
}