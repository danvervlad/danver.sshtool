﻿namespace ConsoleApplication2.Job
{
    public interface IOperationFactory
    {
        Dto.OperationDto CreateOperation(string filePath);
    }
}