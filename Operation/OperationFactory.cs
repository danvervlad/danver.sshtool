﻿using System;
using System.IO;
using ConsoleApplication2.Dto;
using ConsoleApplication2.Job;
using ConsoleApplication2.Json;

namespace ConsoleApplication2.Operation
{
    public class OperationFactory : IOperationFactory
    {
        private readonly IJsonDeserializer _jsonDeserializer;

        public OperationFactory(IJsonDeserializer jsonDeserializer)
        {
            _jsonDeserializer = jsonDeserializer;
        }

        public OperationDto CreateOperation(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new InvalidOperationException(string.Format("OperationFactory: JsonFile '{0}' doesn't exist.", filePath));
            }

            return _jsonDeserializer.DeserializeFile<OperationDto>(filePath);
        }
    }
}