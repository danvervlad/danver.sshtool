﻿using ConsoleApplication2.Dto;

namespace ConsoleApplication2.Stage
{
    public interface IStageFactory
    {
        StageDto CreateStageDto(string filePath);
    }
}