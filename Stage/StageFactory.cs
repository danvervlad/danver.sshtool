﻿using System;
using System.IO;
using ConsoleApplication2.Dto;
using ConsoleApplication2.Json;

namespace ConsoleApplication2.Stage
{
    public class StageFactory : IStageFactory
    {
        private readonly IJsonDeserializer _jsonDeserializer;

        public StageFactory(IJsonDeserializer jsonDeserializer)
        {
            _jsonDeserializer = jsonDeserializer;
        }

        public StageDto CreateStageDto(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new InvalidOperationException(string.Format("StageFactory: JsonFile '{0}' doesn't exist.", filePath));
            }

            return _jsonDeserializer.DeserializeFile<StageDto>(filePath);
        }
    }
}