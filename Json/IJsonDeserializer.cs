﻿namespace ConsoleApplication2.Json
{
    public interface IJsonDeserializer
    {
        T DeserializeJson<T>(string jsonText);
        T DeserializeFile<T>(string filePath);
    }
}