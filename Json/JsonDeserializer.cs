﻿using System.IO;
using Newtonsoft.Json;

namespace ConsoleApplication2.Json
{
    public class JsonDeserializer : IJsonDeserializer
    {
        public T DeserializeJson<T>(string jsonText)
        {
            return JsonConvert.DeserializeObject<T>(jsonText);
        }

        public T DeserializeFile<T>(string filePath)
        {
            return DeserializeJson<T>(File.ReadAllText(filePath));
        }
    }
}