﻿
using System.Collections.Generic;

namespace ConsoleApplication2.Dto
{
    //          {
    //              "StageJsonFile": "StageMf4.json",
    //              "OperationsJsonFiles": [
    //                  "UploadStageMf4Config.json"
    //              ]
    //          }

    public class PackRawDto
    {
        public string StageJsonFile;
        public IList<string> OperationsJsonFiles;
    }
}