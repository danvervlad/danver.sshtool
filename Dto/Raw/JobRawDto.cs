using System.Collections.Generic;

namespace ConsoleApplication2.Dto
{
    //  {
    //      "Name": "Upload Configs",
    //      "Packs": [
    //          {
    //              "StageJsonFile": "StageMf4.json",
    //              "OperationsJsonFiles": [
    //                  "UploadStageMf4Config.json"
    //              ]
    //          }
    //      ]
    //  }

    public class JobRawDto
    {
        public string Name;
        public IList<PackRawDto> Packs;
    }
}