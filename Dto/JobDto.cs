﻿using System.Collections.Generic;

namespace ConsoleApplication2.Dto
{
    public class JobDto
    {
        public readonly string Name;
        public readonly IList<PackDto> Packs;

        public JobDto(string name, IList<PackDto> packs)
        {
            Name = name;
            Packs = packs;
        }
    }
}