﻿namespace ConsoleApplication2.Dto
{
    public class StageDto
    {
        public string Name;
        public string HostName;
        public string Password;
        public string SshHostKeyFingerprint;
        public string UserName;
    }
}