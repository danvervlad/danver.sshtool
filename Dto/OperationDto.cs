namespace ConsoleApplication2.Dto
{
    //  {
    //      "Name": "Upload Config yml",
    //      "Type": "Upload",
    //      "SourcePath": "d://example.txt",
    //      "TargetPath": "/var/www/html/StageDto/vs_crossplatform/config/"
    //  }

    public class OperationDto
    {
        public string Name;
        public string Type;
        public string SourcePath;
        public string TargetPath;
    }
}