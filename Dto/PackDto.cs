﻿using System.Collections.Generic;

namespace ConsoleApplication2.Dto
{
    public class PackDto
    {
        public readonly StageDto StageDto;
        public readonly IList<OperationDto> Operations;

        public PackDto(StageDto stageDto, IList<OperationDto> operations)
        {
            StageDto = stageDto;
            Operations = operations;
        }
    }
}