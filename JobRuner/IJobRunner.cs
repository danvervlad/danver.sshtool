﻿using ConsoleApplication2.Dto;

namespace ConsoleApplication2.JobRuner
{
    public interface IJobRunner
    {
        void RunJob(JobDto jobDto);
    }
}