﻿using System;
using ConsoleApplication2.Dto;
using ConsoleApplication2.Json;
using ConsoleApplication2.Logger;
using WinSCP;

namespace ConsoleApplication2.JobRuner
{
    public class JobRunner : IJobRunner
    {
        private readonly ISessionOptionsFactory _sessionOptionsFactory;
        private readonly ILogger _logger;

        public JobRunner(ISessionOptionsFactory sessionOptionsFactory, ILogger logger)
        {
            _sessionOptionsFactory = sessionOptionsFactory;
            _logger = logger;
        }

        public void RunJob(JobDto jobDto)
        {
            _logger.WriteLine(string.Format("Start Job '{0}'", jobDto.Name));

            foreach (var pack in jobDto.Packs)
            {
                _logger.WriteLine("Begin of pack.");
                try
                {
                    // Setup session options
                    var sessionOptions = _sessionOptionsFactory.CreateSessionOptions(pack.StageDto);

                    using (Session session = new Session())
                    {
                        RunSession(pack, session, sessionOptions);
                    }
                }
                catch (Exception e)
                {
                    _logger.WriteLine(string.Format("Error: {0}", e));
                }
                _logger.WriteLine("End of pack.");
            }

            _logger.WriteLine(string.Format("Job '{0}' is finished.", jobDto.Name));
        }

        private void RunSession(PackDto pack, Session session, SessionOptions sessionOptions)
        {
            _logger.WriteLine(string.Format("Connect to '{0}'", pack.StageDto.Name));

            // Connect
            session.Open(sessionOptions);

            // Upload files
            TransferOptions transferOptions = new TransferOptions();
            transferOptions.TransferMode = TransferMode.Binary;

            foreach (var operation in pack.Operations)
            {
                RunOperation(session, operation, transferOptions);
            }

            session.Close();
            _logger.WriteLine(string.Format("Disconnected from '{0}'", pack.StageDto.Name));
        }

        private void RunOperation(Session session, OperationDto operation, TransferOptions transferOptions)
        {
            _logger.WriteLine(string.Format("Start '{0}' operation '{1}'", operation.Type, operation.Name));

            var transferResult = DoOperation(session, operation, transferOptions);
            CheckOperation(operation, transferResult);

            _logger.WriteLine(string.Format("The '{0}' operation '{1}' is finished. Success: '{2}'", operation.Type,
                operation.Name, transferResult.IsSuccess));
        }

        private void CheckOperation(OperationDto operation, TransferOperationResult transferResult)
        {
            foreach (TransferEventArgs transfer in transferResult.Transfers)
            {
                if (transferResult.IsSuccess)
                {
                    _logger.WriteLine(string.Format("{0} of {1} succeeded", operation.Type, transfer.FileName));
                }
                else
                {
                    _logger.WriteLine(string.Format("{0} of {1} failed. {2}", operation.Type, transfer.FileName,
                        transfer.Error.Message));
                }
            }
        }

        private static TransferOperationResult DoOperation(Session session, OperationDto operation,
            TransferOptions transferOptions)
        {
            TransferOperationResult transferResult;
            if (operation.Type.Equals("Upload"))
            {
                transferResult = session.PutFiles(operation.SourcePath, operation.TargetPath, false, transferOptions);
            }
            else
            {
                transferResult = session.GetFiles(operation.SourcePath, operation.TargetPath, false, transferOptions);
            }

            return transferResult;
        }
    }
}