﻿using ConsoleApplication2.Dto;
using WinSCP;

namespace ConsoleApplication2.Json
{
    public interface ISessionOptionsFactory
    {
        SessionOptions CreateSessionOptions(StageDto stageDto);
    }
}