﻿using ConsoleApplication2.Dto;
using WinSCP;

namespace ConsoleApplication2.Json
{
    public class SessionOptionsFactory : ISessionOptionsFactory
    {
        public SessionOptions CreateSessionOptions(StageDto stageDto)
        {
            return new SessionOptions
            {
                Protocol = Protocol.Sftp,
                HostName = stageDto.HostName,
                UserName = stageDto.UserName,
                Password = stageDto.Password,
                SshHostKeyFingerprint = stageDto.SshHostKeyFingerprint
            };
        }
    }
}