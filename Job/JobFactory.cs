﻿using System;
using System.Collections.Generic;
using System.IO;
using ConsoleApplication2.Dto;
using ConsoleApplication2.Json;
using ConsoleApplication2.Pack;

namespace ConsoleApplication2.Job
{
    public class JobFactory : IJobFactory
    {
        private readonly IJsonDeserializer _jsonDeserializer;
        private readonly IPackFactory _packFactory;

        public JobFactory(IJsonDeserializer jsonDeserializer, IPackFactory packFactory)
        {
            _jsonDeserializer = jsonDeserializer;
            _packFactory = packFactory;
        }

        public JobDto CreateJob(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new InvalidOperationException(string.Format("JobFactory: JsonFile '{0}' doesn't exist.", filePath));
            }

            var jobRawDto = _jsonDeserializer.DeserializeFile<JobRawDto>(filePath);

            var packs = new List<PackDto>(jobRawDto.Packs.Count);
            for (int i = 0; i < jobRawDto.Packs.Count; i++)
            {
                packs.Add(_packFactory.CreatePack(jobRawDto.Packs[i]));
            }

            return new JobDto(jobRawDto.Name, packs);
        }
    }
}