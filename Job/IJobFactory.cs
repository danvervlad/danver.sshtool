﻿namespace ConsoleApplication2.Job
{
    public interface IJobFactory
    {
        Dto.JobDto CreateJob(string packJson);
    }
}