﻿using System;
using System.IO;
using ConsoleApplication2.Logger;
using Newtonsoft.Json.Linq;

namespace ConsoleApplication2.Template
{
    public class ExampleCreator : IExampleCreator
    {
        private readonly ILogger _logger;

        public ExampleCreator(ILogger logger)
        {
            _logger = logger;
        }

        public void CreateTemplates()
        {
            _logger.WriteLine("Start creating examples...");

            var stageJsonFileName1 = "ExampleStage1.json";
            var stageJsonFileName2 = "ExampleStage2.json";

            var operationJsonFileName1 = "ExampleUploadFile1.json";
            var operationJsonFileName2 = "ExampleUploadFiles2.json";
            var operationJsonFileName3 = "ExampleLoadFile3.json";

            var jobJsonFileName1 = "ExampleJob.json";

            var exampleFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Example");
            var stageJsonFileFolder = Path.Combine(exampleFolder, "Stage");
            var operationJsonFileFolder = Path.Combine(exampleFolder, "Operation");
            var jobJsonFileFolder = Path.Combine(exampleFolder, "Job");

            if (!Directory.Exists(exampleFolder))
            {
                Directory.CreateDirectory(exampleFolder);
            }

            // Create Stage Json Config
            if (!Directory.Exists(stageJsonFileFolder))
            {
                Directory.CreateDirectory(stageJsonFileFolder);
            }

            var stageJsonFilePath1 = Path.Combine(stageJsonFileFolder, stageJsonFileName1);
            var stageJsonFilePath2 = Path.Combine(stageJsonFileFolder, stageJsonFileName2);

            File.WriteAllText(stageJsonFilePath1, GetStage1JsonConfig());
            File.WriteAllText(stageJsonFilePath2, GetStage2JsonConfig());

            // Create Operation Json Config
            if (!Directory.Exists(operationJsonFileFolder))
            {
                Directory.CreateDirectory(operationJsonFileFolder);
            }

            var operationJsonFilePath1 = Path.Combine(operationJsonFileFolder, operationJsonFileName1);
            var operationJsonFilePath2 = Path.Combine(operationJsonFileFolder, operationJsonFileName2);
            var operationJsonFilePath3 = Path.Combine(operationJsonFileFolder, operationJsonFileName3);

            File.WriteAllText(operationJsonFilePath1, GetOperation1JsonConfig());
            File.WriteAllText(operationJsonFilePath2, GetOperation2JsonConfig());
            File.WriteAllText(operationJsonFilePath3, GetOperation3JsonConfig());

            // Create Job Json Config
            if (!Directory.Exists(jobJsonFileFolder))
            {
                Directory.CreateDirectory(jobJsonFileFolder);
            }

            File.WriteAllText(Path.Combine(jobJsonFileFolder, jobJsonFileName1),
                GetJob1JsonConfig(stageJsonFilePath1, stageJsonFilePath2, operationJsonFilePath1, operationJsonFilePath2,
                    operationJsonFilePath3));

            _logger.WriteLine(string.Format("Examples are created at '{0}'. Check it out.", exampleFolder));
        }

        private string GetStage1JsonConfig()
        {
            var jObject = new JObject(
                            new JProperty("Name", "Example Stage 1"),
                            new JProperty("HostName", "my.stage1.com"),
                            new JProperty("UserName", "admin"),
                            new JProperty("Password", "adminpass"),
                            new JProperty("SshHostKeyFingerprint", "ssh -rsa 2048 12:12:12:12:12:12:12:12"));

            return jObject.ToString();
        }

        private string GetStage2JsonConfig()
        {
            var jObject = new JObject(
                            new JProperty("Name", "Example Stage 2"),
                            new JProperty("HostName", "my.stage2.com"),
                            new JProperty("UserName", "admin"),
                            new JProperty("Password", "adminpass"),
                            new JProperty("SshHostKeyFingerprint", "ssh -rsa 2048 13:13:13:13:13:13:13:13"));

            return jObject.ToString();
        }

        private string GetOperation1JsonConfig()
        {
            var jObject = new JObject(
                            new JProperty("Name", "Upload Example file"),
                            new JProperty("Type", "Upload"),
                            new JProperty("SourcePath", @"d:\example.txt"),
                            new JProperty("TargetPath", "/var/"));

            return jObject.ToString();
        }


        private string GetOperation2JsonConfig()
        {
            var jObject = new JObject(
                            new JProperty("Name", "Upload All Example files"),
                            new JProperty("Type", "Upload"),
                            new JProperty("SourcePath", @"d:\ExampleFiles\*.*"),
                            new JProperty("TargetPath", "/var/ExampleFiles/"));

            return jObject.ToString();
        }

        private string GetOperation3JsonConfig()
        {
            var jObject = new JObject(
                            new JProperty("Name", "Load Example file"),
                            new JProperty("Type", "Load"),
                            new JProperty("SourcePath", "/var/example.txt"),
                            new JProperty("TargetPath", @"d:\example.txt"));

            return jObject.ToString();
        }

        private string GetJob1JsonConfig(string stageJsonFilePath1, string stageJsonFilePath2, string operationPath1,
            string operationPath2, string operationPath3)
        {
            var jObject = new JObject(
                            new JProperty("Name", "Example Job"),
                            new JProperty("Packs",
                                new JArray(
                                    new JObject(
                                        new JProperty("StageJsonFile", stageJsonFilePath1),
                                        new JProperty("OperationsJsonFiles", 
                                            new JArray(new JValue(operationPath1)))),
                                    new JObject(
                                        new JProperty("StageJsonFile", stageJsonFilePath2),
                                        new JProperty("OperationsJsonFiles", 
                                            new JArray(new JValue(operationPath2), new JValue(operationPath3)))))));

            return jObject.ToString();
        }
    }
}